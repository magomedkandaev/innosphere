module.exports = function(grunt) {

  grunt.initConfig({
    watch: {
            files: 'app/sass/**/*.sass',
            tasks: ['sass']
        },
        sass: {
            dist: {
                files: {
                    'app/css/main.css': 'app/sass/style.sass'
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        './app/css/*.css',
                        './app/*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                      baseDir: "./app",
                      routes: {
                        "/node_modules": "node_modules"
                    }
                  }
                }
            }
        }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.registerTask('default', ['sass', 'browserSync', 'watch']);

};